﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Entities;
using WindowsFormsApplication1.model;

namespace WindowsFormsApplication1
{
    public partial class FrmMain : Form
    {
        private DataTable dtUsuario;
        public FrmMain()
        {
            InitializeComponent();
        }

        private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestion frmg = new FrmGestion();
            frmg.MdiParent = this;
            frmg.DsUsuarios = dsUsuario;
            frmg.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            usuarioModel.populate();
            dtUsuario = dsUsuario.Tables["Usuario"];
            foreach(usuario user in usuarioModel.GetUsuario()){
                dtUsuario.Rows.Add(user.Nombre_usuario,user.Correo,user.Contraseña,user.Telefono); 
            }
                      
        }
    }
}
