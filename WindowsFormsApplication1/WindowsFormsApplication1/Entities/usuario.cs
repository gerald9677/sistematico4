﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Entities
{
    class usuario
    {
        private int id;
        private string nombre_usuario;
        private string contraseña;
        private string confirmar_contraseña;
        private string correo;
        private string confirmar_correo;
        private int telefono;
        private int confirmar_telefono;
        private int cantidad_accesos_fallidos;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre_usuario
        {
            get
            {
                return nombre_usuario;
            }

            set
            {
                nombre_usuario = value;
            }
        }

        public string Contraseña
        {
            get
            {
                return contraseña;
            }

            set
            {
                contraseña = value;
            }
        }

        public string Confirmar_contraseña
        {
            get
            {
                return confirmar_contraseña;
            }

            set
            {
                confirmar_contraseña = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Confirmar_correo
        {
            get
            {
                return confirmar_correo;
            }

            set
            {
                confirmar_correo = value;
            }
        }

        public int Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public int Confirmar_telefono
        {
            get
            {
                return confirmar_telefono;
            }

            set
            {
                confirmar_telefono = value;
            }
        }

        public int Cantidad_accesos_fallidos
        {
            get
            {
                return cantidad_accesos_fallidos;
            }

            set
            {
                cantidad_accesos_fallidos = value;
            }
        }

        public usuario(int id, string nombre_usuario, string contraseña, string confirmar_contraseña, string correo, string confirmar_correo, int telefono, int confirmar_telefono, int cantidad_accesos_fallidos)
        {
            this.Id = id;
            this.Nombre_usuario = nombre_usuario;
            this.Contraseña = contraseña;
            this.Confirmar_contraseña = confirmar_contraseña;
            this.Correo = correo;
            this.Confirmar_correo = confirmar_correo;
            this.Telefono = telefono;
            this.Confirmar_telefono = confirmar_telefono;
            this.Cantidad_accesos_fallidos = cantidad_accesos_fallidos;
        }
    }
}
